<?php
	require_once('Person.php');
	
		class User extends Person{
			private $department;
			
			public function __construct($name,$department){
			$this->name = $name;
			$this->department = $department;
			}
			
			public function getName(){
				return $this->name;
			}
			
			public function getDescription(){
				return parent::getDescription()." Gilt als User. ";
			}
			
			public function getDepartment(){
				return "Arbeitet in Abteilung: ".$this->department.". ";
			}
		}
?>
